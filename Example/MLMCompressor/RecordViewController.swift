//
//  RecordViewController.swift
//  MLMCompressor_Example
//
//  Created by Do Thanh Tung on 20/10/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Compression
import MLMCompressor
import AVKit

class RecordViewController: UIViewController {
    //    var frameExtractor: FrameExtractor!
    var quality = VideoQuality.very_low
    var encodeType = AVVideoCodecType.h264
    private var permissionGranted = false
    let captureSession = AVCaptureSession()
    let captureQueue = DispatchQueue(label: "videotoolbox.compression.capture")
    lazy var preview: AVCaptureVideoPreviewLayer = {
        let preview = AVCaptureVideoPreviewLayer(session: self.captureSession)
        preview.videoGravity = .resizeAspectFill
        view.layer.addSublayer(preview)
        
        return preview
    }()
    
    var isRecording = false
    private var compression: MLMComression?
    private var output: VideoResult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkPermission()
        
        let device = AVCaptureDevice.default(for: .video)!
        let input = try! AVCaptureDeviceInput(device: device)
        if captureSession.canAddInput(input) {
            captureSession.addInput(input)
        }
        captureSession.sessionPreset = .high
        let output = AVCaptureVideoDataOutput()
        // YUV 420v
        output.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange]
        output.setSampleBufferDelegate(self, queue: captureQueue)
        if captureSession.canAddOutput(output) {
            captureSession.addOutput(output)
        }
        
        if let connection = output.connection(with: .video) {
            if connection.isVideoOrientationSupported {
                connection.videoOrientation = AVCaptureVideoOrientation(rawValue: UIApplication.shared.statusBarOrientation.rawValue)!
            }
        }
        
        
        preview.frame = view.bounds
        self.view.layer.addSublayer(preview)
        let button = UIButton(type: .roundedRect)
        button.setTitle("Record", for: .normal)
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(recordPressed), for: .touchUpInside)
        button.frame = CGRect(x: 100, y: 200, width: 110, height: 40)
        button.center = view.center
        view.addSubview(button)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived), name: NSNotification.Name.AVCaptureSessionRuntimeError, object: nil)
        
        compression = MLMComression(quality: quality, outputType: encodeType)
        compression?.completionHandler = { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            case .onStart:
                print("START")
            case .onCompressing(let process):
                guard let process = process else { return }
                print(process)
            case .onFailure(let error):
                print(error)
            case .onSuccess(let output):
                DispatchQueue.main.async {
                    self.handleVideoOutput(output)
                }
            case .onCancelled:
                print("CANCELED")
            }
        }
    }
    
    func handleVideoOutput(_ videoOutput: VideoResult?) {
        guard let output = videoOutput else {
            return
        }
        self.output = output
        preview.removeFromSuperlayer()
        let rect = self.view.bounds
        let imageView = UIImageView(frame: rect)
        imageView.image = output.thumbnail
        imageView.contentMode = .scaleAspectFill
        self.view.addSubview(imageView)
        
        let buttonPlay = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 50)))
        buttonPlay.backgroundColor = .blue
        buttonPlay.setTitle("PLay", for: .normal)
        buttonPlay.center = view.center
        buttonPlay.addTarget(self, action: #selector(playVideo), for: .touchUpInside)
        self.view.addSubview(buttonPlay)
        
        let saveThumb = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 150, height: 50)))
        saveThumb.backgroundColor = .blue
        saveThumb.setTitle("Save Thumb", for: .normal)
        saveThumb.center = CGPoint(x: view.center.x, y: view.center.y + 70)
        saveThumb.addTarget(self, action: #selector(saveImageButtonAction), for: .touchUpInside)
        self.view.addSubview(saveThumb)
        
        let labelDisplay = UILabel()
        labelDisplay.frame = CGRect(x: 0, y: view.center.y + 50, width: rect.width, height: rect.height / 2)
        labelDisplay.numberOfLines = 0
        labelDisplay.textColor = .red
        var info = "videoEncodeType: \(output.videoEncodeType.rawValue)"
        info += "\n video quality: \(output.videoQuality)"
        info += "\n video rate: \(getSize(UInt64(output.saveBitRate)))"
        info += "\n video duration: \(output.duration)"
        info += "\n video width: \(output.videoWith)"
        info += "\n video height: \(output.videoHeight)"
        info += "\n video file size: \(getSize(output.fileSize))"
        
        labelDisplay.text = info
        self.view.addSubview(labelDisplay)
    }
    
    @objc func playVideo() {
        let player = AVPlayer(url: (output.asset as! AVURLAsset).url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    @objc func notificationReceived(notification: NSNotification){
        print(notification)
    }
    
    func captured(frame: CMSampleBuffer) {
        if isRecording {
            compression?.capture(frame)
        }
    }
    
    @IBAction func recordPressed(_ sender: Any) {
        guard permissionGranted else { return }
        let button = sender as? UIButton
        if isRecording {
            // Stop
            isRecording = false
            compression?.stopRecord()
            captureSession.stopRunning()
            button?.setTitle("Record", for: .normal)
        } else {
            // init and start
            button?.setTitle("Stop Recording", for: .normal)
            isRecording = true
            captureSession.sessionPreset = .high
            captureSession.startRunning()
        }
    }
    
    private func checkPermission() {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            permissionGranted = true
        case .notDetermined:
            requestPermission()
        default:
            permissionGranted = false
        }
    }
    
    private func requestPermission() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { [unowned self] granted in
            self.permissionGranted = granted
        }
    }
    
    @objc func saveImageButtonAction() {
        guard let image = output.thumbnail else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            self.presentAlert("Error: \(error.localizedDescription)")
        }
        else {
            self.presentAlert("Saved to Photo")
        }
    }
    
    func presentAlert(_ mes: String) {
        let alrtC = UIAlertController(title: "Alert", message: mes, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            alrtC.dismiss(animated: true, completion: nil)
        }
        alrtC.addAction(okAction)
        present(alrtC, animated: true, completion: nil)
    }
    
    func getSize(_ sizeCount: UInt64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = .useMB
        formatter.countStyle = .file
        return formatter.string(fromByteCount: Int64(sizeCount))
    }
}
extension RecordViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ captureOutput: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        DispatchQueue.main.async { [unowned self] in
            self.captured(frame: sampleBuffer)
        }
    }
}
