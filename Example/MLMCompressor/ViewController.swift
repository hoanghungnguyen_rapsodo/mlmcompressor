//
//  ViewController.swift
//  MLMCompressor
//
//  Created by Hung Nguyen on 10/14/2021.
//  Copyright (c) 2021 Hung Nguyen. All rights reserved.
//

import UIKit
import MLMCompressor
import AVFoundation

class ViewController: UIViewController {
    
    var quality = VideoQuality.very_low
    var encodeType = AVVideoCodecType.hevc
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func pushToCompressVideo(_ sender: UIButton) {
        let cViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CompressViewController") as? CompressViewController
        cViewController?.quality = quality
        cViewController?.encodeType = encodeType
        self.navigationController?.pushViewController(cViewController!, animated: true)
    }
    
    @IBAction func pushToRecordVideo(_ sender: UIButton) {
        let rViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecordViewController") as? RecordViewController
        rViewController?.quality = quality
        rViewController?.encodeType = encodeType
        self.navigationController?.pushViewController(rViewController!, animated: true)
    }
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            quality = .very_low
        case 1:
            quality = .low
        case 2:
            quality = .medium
        case 3:
            quality = .high
        case 4:
            quality = .very_high
        case 5:
            quality = .original
        default:
            quality = .very_low
        }
    }
}

