//
//  CompressViewController.swift
//  MLMCompressor_Example
//
//  Created by Do Thanh Tung on 20/10/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import MLMCompressor
import AVFoundation
import AVKit
import QuartzCore

class CompressViewController: UIViewController {
    var quality = VideoQuality.very_low
    var encodeType = AVVideoCodecType.h264
    @IBOutlet var percenLabel: UILabel!
    
    private var compression: MLMComression?
    private var videoURL: URL!
    private var output: VideoResult!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let file = "test_video_h264.mp4".components(separatedBy: ".")
        
        guard let path = Bundle.main.path(forResource: file[0], ofType:file[1]) else {
            debugPrint( "\(file.joined(separator: ".")) not found")
            return
        }
        videoURL = URL(fileURLWithPath: path)
    }
    
    @IBAction func cancelExport(_ sender: UIButton) {
        compression?.cancelExport()
    }
    
    @IBAction func compress(_ sender: UIButton) {
        compression = MLMComression(asset: AVAsset(url: videoURL),
                                    quality: quality,
                                    outputType: encodeType)
        
        compression?.completionHandler = { [weak self] (result) in
            guard let strongSelf = self else { return }
            switch result {
            case .onStart:
                DispatchQueue.main.async {
                    strongSelf.percenLabel.text = "Start"
                }
                print("START")
                
            case .onCompressing(let process):
                guard let process = process else { return }
                DispatchQueue.main.async {
                    strongSelf.percenLabel.text = "\(Int(process * 100))%"
                }
                print("\(Int(process * 100))%")
            case .onFailure(let error):
                DispatchQueue.main.async {
                    strongSelf.percenLabel.text = error.localizedDescription
                }
                print("ERROR: \(error.localizedDescription)")
            case .onSuccess(let output):                
                DispatchQueue.main.async {
                    strongSelf.handleVideoOutput(output)
                }
                print("SUCCESS")
            case .onCancelled:
                DispatchQueue.main.async {
                    strongSelf.percenLabel.text = "Cancel"
                }
                print("Canceled")
            }
        }
        compression?.export()
    }
    
    func handleVideoOutput(_ videoOutput: VideoResult?) {
        guard let output = videoOutput else {
            return
        }
        self.output = output
        let rect = self.view.bounds
        let imageView = UIImageView(frame: rect)
        let thump = output.thumbnail
        imageView.image = thump
//        imageView.image = compression?.generateThumbnailFromAsset(asset: output.asset, fromTime: 3)
        imageView.contentMode = .scaleAspectFill
        self.view.addSubview(imageView)
                
        let buttonPlay = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 50)))
        buttonPlay.backgroundColor = .blue
        buttonPlay.setTitle("PLay", for: .normal)
        buttonPlay.center = view.center
        buttonPlay.addTarget(self, action: #selector(playVideo), for: .touchUpInside)
        self.view.addSubview(buttonPlay)
        
        let saveThumb = UIButton(frame: CGRect(origin: .zero, size: CGSize(width: 150, height: 50)))
        saveThumb.backgroundColor = .blue
        saveThumb.setTitle("Save Thumb", for: .normal)
        saveThumb.center = CGPoint(x: view.center.x, y: view.center.y + 70)
        saveThumb.addTarget(self, action: #selector(saveImageButtonAction), for: .touchUpInside)
        self.view.addSubview(saveThumb)
        
        let labelDisplay = UILabel()
        labelDisplay.frame = CGRect(x: 0, y: view.center.y + 50, width: rect.width, height: rect.height / 2)
        labelDisplay.numberOfLines = 0
        labelDisplay.textColor = .red
        var info = "videoEncodeType: \(output.videoEncodeType.rawValue)"
        info += "\n video quality: \(output.videoQuality)"
        info += "\n video rate: \(getSize(UInt64(output.saveBitRate)))"
        info += "\n video duration: \(output.duration)"
        info += "\n video width: \(output.videoWith)"
        info += "\n video height: \(output.videoHeight)"
        info += "\n video file size: \(getSize(output.fileSize))"
        
        labelDisplay.text = info
        self.view.addSubview(labelDisplay)
    }
    
    @objc func playVideo() {
        let player = AVPlayer(url: (output.asset as! AVURLAsset).url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    @objc func saveImageButtonAction() {
        guard let image = output.thumbnail else { return }
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            self.presentAlert("Error: \(error.localizedDescription)")
        }
        else {
            self.presentAlert("Saved to Photo")
        }
    }
    
    func presentAlert(_ mes: String) {
        let alrtC = UIAlertController(title: "Alert", message: mes, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            alrtC.dismiss(animated: true, completion: nil)
        }
        alrtC.addAction(okAction)
        present(alrtC, animated: true, completion: nil)
    }
    
    func getSize(_ sizeCount: UInt64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = .useMB
        formatter.countStyle = .file
        return formatter.string(fromByteCount: Int64(sizeCount))
    }
}

