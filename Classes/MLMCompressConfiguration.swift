import AVFoundation

public enum VideoQuality {
    case original // keep 100% bitrate
    case very_high // keep 90% bitrate
    case high // keep 70% bitrate
    case medium // keep 50% bitrate
    case low // keep 30% bitrate
    case very_low // keep 10% bitrate
}

public class ExportConfiguration {
    public var outputURL: URL?
    public var fileType: AVFileType = .mp4
    public var shouldOptimizeForNetworkUse = false
    public var metadata: [AVMetadataItem] = []
}

public class VideoRecordingConfiguration {
    public var videoOutputSetting: [String: Any]?
}

public class VideoConfiguration {
    public var videoInputSetting: [String: Any]?
    public var videoOutputSetting: [String: Any]?
    public var videoComposition: AVVideoComposition?
}

public class AudioConfiguration {
    public var audioInputSetting: [String: Any]?
    public var audioOutputSetting: [String: Any]?
    public var audioMix: AVAudioMix?
    public var audioTimePitchAlgorithm: AVAudioTimePitchAlgorithm?
}

public struct VideoResult {
    public var saveBitRate: Int
    public var videoWith: Int
    public var videoHeight: Int
    public var thumbnail: UIImage?
    public var asset: AVAsset
    public var compressedFilePath: String
    public var fileSize: UInt64
    public var duration: Double
    public var videoEncodeType: AVVideoCodecType
    public var videoQuality: VideoQuality
}

// Compression Result
public enum CompressionResult {
    case onStart
    case onSuccess(VideoResult?)
    case onCompressing(Float?)
    case onFailure(NSError)
    case onCancelled
}

// Compression Interruption Wrapper
public class Compression {
    public init() {}

    public var cancel = false
}

struct MLMCompressConfiguration {
    static let MIN_BITRATE = Float(2000000)
    static let MIN_HEIGHT = 640.0
    static let MIN_WIDTH = 360.0
    
    static let direction = MLMCompressConfiguration.directoryForNewVideo()
    
    static func directoryForNewVideo() -> URL {
        let videoDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("videos")
        try? FileManager.default.createDirectory(atPath: (videoDir?.path)!, withIntermediateDirectories: true, attributes: nil)
        return videoDir!
    }
}
