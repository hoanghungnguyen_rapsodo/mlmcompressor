
import AVFoundation

@available(iOS 11.0, *)
public class MLMComression {
    public private(set) var asset: AVAsset?
    public var exportConfiguration = ExportConfiguration()
    public var videoRecordingConfiguration = VideoRecordingConfiguration()
    public var videoConfiguration = VideoConfiguration()
    public var audioConfiguration = AudioConfiguration()
    
    private var reader: AVAssetReader?
    private var videoOutput: AVAssetReaderVideoCompositionOutput?
    private var audioOutput: AVAssetReaderAudioMixOutput?
    private var writer: AVAssetWriter?
    private var videoInput: AVAssetWriterInput?
    private var audioInput: AVAssetWriterInput?
    private var recordInput: AVAssetWriterInput?
    private var inputQueue = DispatchQueue(label: "VideoEncoderQueue", qos: .background)
    private var lastVideoSamplePresentationTime = CMTime.zero
    private var lastAudioSamplePresentationTime = CMTime.zero
    
    var saveBitRate: Int = 0
    var videoWith: Int = 0
    var videoHeight: Int = 0
    
    // MARK: - Exporting properties
    public var progress: Float? {
        didSet {
            if let process = progress {
                completionHandler?(.onCompressing(process))
            }
        }
    }
    public var videoProgress: Float? {
        didSet {
            if audioInput != nil {
                progress = 0.95 * (videoProgress ?? 0) + 0.05 * (audioProgress ?? 0)
            } else {
                progress = videoProgress
            }
        }
    }
    public var audioProgress: Float? {
        didSet {
            if videoInput != nil {
                progress = 0.95 * (videoProgress ?? 0) + 0.05 * (audioProgress ?? 0)
            } else {
                progress = audioProgress
            }
        }
    }
    
    public var videoQuality = VideoQuality.medium
    public var isMinBitRateEnabled = false
    public var keepOriginalResolution = true
    public var outputType = AVVideoCodecType.hevc
    public var fileName = "compress_video.mp4"
    public var completionHandler: ((CompressionResult) -> Void)?
    
    fileprivate var videoCompleted = false
    fileprivate var audioCompleted = false
    
    public init(asset: AVAsset? = nil, fileName: String? = nil, quality: VideoQuality = VideoQuality.medium, isMinBitRateEnabled: Bool = false, keepOriginalResolution: Bool = true, outputType: AVVideoCodecType = .hevc) {
        if let asset = asset {
            self.asset = asset
        }
        if let fName = fileName {
            self.fileName = fName
        }
        self.videoQuality = quality
        self.isMinBitRateEnabled = isMinBitRateEnabled
        self.keepOriginalResolution = keepOriginalResolution
    }
    
    private func buildRecordConfiguration(_ firstSampleBuffer: CMSampleBuffer) {
        do {
            if videoRecordingConfiguration.videoOutputSetting == nil {
                videoRecordingConfiguration.videoOutputSetting = try buildDefaulVideoRecordingOutputSetting(firstSampleBuffer)
            }
            self.removeExistFile(fileName)
            exportConfiguration.outputURL = MLMCompressConfiguration.direction.appendingPathComponent(fileName)
            
        } catch let error as NSError {
            completionHandler?(.onFailure(error))
        }
        
    }
    
    private func buildConfiguration() {
        do {
            if audioConfiguration.audioOutputSetting == nil {
                audioConfiguration.audioOutputSetting = buildDefaultAudioOutputSetting()
            }
            
            if videoConfiguration.videoOutputSetting == nil {
                videoConfiguration.videoOutputSetting = try buildDefaultVideoOutputSetting()
            }
            self.removeExistFile(fileName)
            exportConfiguration.outputURL = MLMCompressConfiguration.direction.appendingPathComponent(fileName)
            
        } catch let error as NSError {
            completionHandler?(.onFailure(error))
        }
    }
}

// MARK:- Export
@available(iOS 11.0, *)
extension MLMComression {
    public func export() {
        reset()
        do {
            guard let asset = asset else {
                let eror = NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Asset invalid", comment: "")])
                throw eror
            }
            
            buildConfiguration()
            reader = try AVAssetReader(asset: asset)
            writer = try AVAssetWriter(url: exportConfiguration.outputURL!, fileType: exportConfiguration.fileType)
            writer?.shouldOptimizeForNetworkUse = exportConfiguration.shouldOptimizeForNetworkUse
            writer?.metadata = exportConfiguration.metadata
            
            // Video output
            let videoTracks = asset.tracks(withMediaType: .video)
            if videoTracks.count > 0 {
                
                let videoOutput = AVAssetReaderVideoCompositionOutput(videoTracks: videoTracks, videoSettings: videoConfiguration.videoInputSetting)
                videoOutput.alwaysCopiesSampleData = false
                videoOutput.videoComposition = videoConfiguration.videoComposition
                if videoOutput.videoComposition == nil {
                    videoOutput.videoComposition = buildDefaultVideoComposition(with: asset)
                }
                
                guard reader?.canAdd(videoOutput) ?? false else {
                    throw NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Can't add video output", comment: "")])
                }
                reader?.add(videoOutput)
                self.videoOutput = videoOutput
                
                // Video input
                let videoInput = AVAssetWriterInput(mediaType: .video, outputSettings: videoConfiguration.videoOutputSetting)
                videoInput.expectsMediaDataInRealTime = false
                guard writer?.canAdd(videoInput) ?? false else {
                    throw NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Can't add video input", comment: "")])
                }
                writer?.add(videoInput)
                self.videoInput = videoInput
            }
            
            // Audio output
            let audioTracks = asset.tracks(withMediaType: .audio)
            if audioTracks.count > 0 {
                
                let audioOutput = AVAssetReaderAudioMixOutput(audioTracks: audioTracks, audioSettings: audioConfiguration.audioInputSetting)
                audioOutput.alwaysCopiesSampleData = false
                audioOutput.audioMix = audioConfiguration.audioMix
                if let audioTimePitchAlgorithm = audioConfiguration.audioTimePitchAlgorithm {
                    audioOutput.audioTimePitchAlgorithm = audioTimePitchAlgorithm
                }
                if reader?.canAdd(audioOutput) ?? false {
                    reader?.add(audioOutput)
                    self.audioOutput = audioOutput
                }
                
                if self.audioOutput != nil {
                    // Audio input
                    let audioInput = AVAssetWriterInput(mediaType: .audio, outputSettings: audioConfiguration.audioOutputSetting)
                    audioInput.expectsMediaDataInRealTime = false
                    if writer?.canAdd(audioInput) ?? false {
                        writer?.add(audioInput)
                        self.audioInput = audioInput
                    }
                }
            }
            completionHandler?(.onStart)
            writer?.startWriting()
            reader?.startReading()
            writer?.startSession(atSourceTime: CMTime.zero)
            
            encodeVideoData()
            encodeAudioData()
        } catch let error as NSError {
            self.completionHandler?(.onFailure(error))
        }
    }
    
    fileprivate func encodeVideoData() {
        if let videoInput = videoInput {
            videoInput.requestMediaDataWhenReady(on: inputQueue, using: { [weak self] in
                guard let self = self else { return }
                guard let videoOutput = self.videoOutput, let videoInput = self.videoInput else { return }
                self.encodeReadySamplesFrom(output: videoOutput, to: videoInput, completion: {
                    self.videoCompleted = true
                    self.tryFinish()
                })
            })
        } else {
            videoCompleted = true
            tryFinish()
        }
    }
    
    fileprivate func encodeAudioData() {
        if let audioInput = audioInput {
            audioInput.requestMediaDataWhenReady(on: inputQueue, using: { [weak self] in
                guard let self = self else { return }
                guard let audioOutput = self.audioOutput, let audioInput = self.audioInput else { return }
                self.encodeReadySamplesFrom(output: audioOutput, to: audioInput, completion: {
                    self.audioCompleted = true
                    self.tryFinish()
                })
            })
        } else {
            audioCompleted = true
            tryFinish()
        }
    }
    
    private func encodeReadySamplesFrom(output: AVAssetReaderOutput, to input: AVAssetWriterInput, completion: @escaping () -> Void) {
        guard let asset = asset else {
            let eror = NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Asset invalid", comment: "")])
            completionHandler?(.onFailure(eror))
            return
        }
        
        while input.isReadyForMoreMediaData {
            let complete = autoreleasepool(invoking: { [weak self] () -> Bool in
                guard let self = self else { return true }
                if let sampleBuffer = output.copyNextSampleBuffer() {
                    guard self.reader?.status == .reading && self.writer?.status == .writing else {
                        return true
                    }
                    
                    guard input.append(sampleBuffer) else {
                        return true
                    }
                    
                    if let videoOutput = self.videoOutput, videoOutput == output {
                        lastVideoSamplePresentationTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
                        if asset.duration.seconds > 0 {
                            self.videoProgress = Float(lastVideoSamplePresentationTime.seconds / asset.duration.seconds)
                        } else {
                            self.videoProgress = 1
                        }
                    } else if let audioOutput = self.audioOutput, audioOutput == output {
                        lastAudioSamplePresentationTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
                        if asset.duration.seconds > 0 {
                            self.audioProgress = Float(lastAudioSamplePresentationTime.seconds / asset.duration.seconds)
                        } else {
                            self.audioProgress = 1
                        }
                    }
                } else {
                    input.markAsFinished()
                    return true
                }
                return false
            })
            if complete {
                completion()
                break
            }
        }
    }
}


@available(iOS 11.0, *)
extension MLMComression {
    private func tryFinish() {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if audioCompleted && videoCompleted {
            if reader?.status == .cancelled || writer?.status == .cancelled {
                finish()
            } else if writer?.status == .failed {
                finish()
            } else if reader?.status == .failed {
                writer?.cancelWriting()
                finish()
            } else {
                writer?.finishWriting { [weak self] in
                    guard let self = self else { return }
                    self.finish()
                }
            }
        }
    }
    
    private func finish() {
        if let filePath = exportConfiguration.outputURL, (writer?.status == .failed || reader?.status == .failed) {
            self.removeFileAt(filePath)
        }
        if let error = (writer?.error ?? reader?.error) {
            completionHandler?(.onFailure(error as NSError))
        } else {
            completionHandler?(.onSuccess(makeVideoResult()))
        }
        reset()
    }
    
    public func cancelExport() {
        if let writer = writer, let reader = reader {
            inputQueue.async {
                writer.cancelWriting()
                reader.cancelReading()
            }
        }
        
        if let filePath = exportConfiguration.outputURL {
            self.removeFileAt(filePath)
        }
        
        videoProgress = nil
        audioProgress = nil
        progress = nil
        
        completionHandler?(.onCancelled)
    }
    
    private func reset() {
        if let writer = writer, let reader = reader {
            inputQueue.async {
                writer.cancelWriting()
                reader.cancelReading()
            }
        }
        videoCompleted = false
        videoCompleted = false
        
        
        reader = nil
        videoOutput = nil
        audioInput = nil
        writer = nil
        videoInput = nil
        audioInput = nil
        
        videoProgress = nil
        audioProgress = nil
        progress = nil
    }
    
    private func makeVideoResult() -> VideoResult? {
        guard let outpuPath = exportConfiguration.outputURL else {
            return nil
        }
        do {
            let asset = AVAsset(url: outpuPath)
            let image = generateThumbnailFromAsset(asset: asset, fromTime: 1)
            let attr = try FileManager.default.attributesOfItem(atPath: outpuPath.path)
            let fileSize = attr[FileAttributeKey.size] as! UInt64
            let duration = asset.duration.seconds
            return VideoResult(saveBitRate: saveBitRate,
                               videoWith: videoWith,
                               videoHeight: videoHeight,
                               thumbnail: image,
                               asset: asset,
                               compressedFilePath: outpuPath.path,
                               fileSize: fileSize,
                               duration: duration,
                               videoEncodeType: outputType,
                               videoQuality: videoQuality)
            
        } catch let error as NSError {
            completionHandler?(.onFailure(error))
        }
        return nil
    }
    
    public func generateThumbnailFromAsset(asset: AVAsset, fromTime: Float64) -> UIImage? {
        guard fromTime < asset.duration.seconds else {
            return nil
        }
        do {
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            assetImgGenerate.requestedTimeToleranceAfter = CMTime.zero
            assetImgGenerate.requestedTimeToleranceBefore = CMTime.zero
            let time        : CMTime = CMTimeMakeWithSeconds(fromTime, preferredTimescale: 10)
            let img         : CGImage = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let frameImg = UIImage(cgImage: img)
            if let frameImgData = frameImg.jpegData(compressionQuality: 1) {
                return UIImage(data: frameImgData)
            } else {
                return frameImg
            }
        } catch let error as NSError {
            completionHandler?(.onFailure(error))
        }
        return nil
    }
}

// MARK:- Reconding
@available(iOS 11.0, *)
extension MLMComression {
    public func capture(_ sampleBuffer: CMSampleBuffer) {
        do {
            if recordInput == nil {
                buildRecordConfiguration(sampleBuffer)
                let videoInput = AVAssetWriterInput(mediaType: .video, outputSettings: videoRecordingConfiguration.videoOutputSetting)
                videoInput.expectsMediaDataInRealTime = true
                writer = try AVAssetWriter(url: exportConfiguration.outputURL!, fileType: exportConfiguration.fileType)
                guard writer?.canAdd(videoInput) ?? false else {
                    throw NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Can't add video input", comment: "")])
                }
                writer?.add(videoInput)
                self.recordInput = videoInput
            }
            recordWrite(sampleBuffer: sampleBuffer)
        } catch let error as NSError {
            self.completionHandler?(.onFailure(error))
        }
    }
    
    func recordWrite(sampleBuffer buffer: CMSampleBuffer) {
        guard let recordInput = self.recordInput else {
            return
        }
        if writer?.status == AVAssetWriter.Status.unknown {
            writer?.startWriting()
            writer?.startSession(atSourceTime: CMSampleBufferGetPresentationTimeStamp(buffer))
        }
        if recordInput.isReadyForMoreMediaData {
            recordInput.append(buffer)
        }
    }
    
    public func stopRecord() {
        if let writer = writer {
            writer.finishWriting { [weak self] in
                guard let self = self else {
                    return
                }
                self.finish()
            }
        }
    }
}

// MARK:- Remove and folder if need
@available(iOS 11.0, *)
extension MLMComression {
    private func removeExistFile(_ fileName: String) {
        let documentsURL = MLMCompressConfiguration.direction
        while checkExistFile(fileName) {
            self.removeFileAt(documentsURL.appendingPathComponent(fileName))
        }
    }
    
    private func checkExistFile(_ fileName: String) -> Bool {
        let documentsURL = MLMCompressConfiguration.direction
        let path = documentsURL.appendingPathComponent(fileName).path
        if FileManager.default.fileExists(atPath: path) {
            return true
        } else {
            return false
        }
    }
    
    public func removeFileAt(_ filePath: URL) {
        try? FileManager.default.removeItem(at: filePath)
    }
    
    public func removeAllFile() {
        let documentsURL = MLMCompressConfiguration.direction
        try? FileManager.default.removeItem(at: documentsURL)
        createDirectoryForNewVideo()
    }
    
    private func createDirectoryForNewVideo() {
        let videoDir = MLMCompressConfiguration.direction
        try? FileManager.default.createDirectory(atPath: (videoDir.path), withIntermediateDirectories: true, attributes: nil)
    }
}
