
import AVFoundation

// MARK:- BUILD SETTING
@available(iOS 11.0, *)
extension MLMComression {
    
    func buildDefaulVideoRecordingOutputSetting(_ firstSampleBuffer: CMSampleBuffer) throws ->  [String: Any]  {
        guard let pixelbuffer = CMSampleBufferGetImageBuffer(firstSampleBuffer) else {
            throw NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Cannot record video", comment: "")])
        }
        videoWith = CVPixelBufferGetWidth(pixelbuffer)
        videoHeight = CVPixelBufferGetHeight(pixelbuffer)
        
        // AVVideoAverageBitRateKey is for pecifying a key to access the average bit rate (as bits per second) used in encoding.
                // This video shoule be video size * a float number, and here 10.1 is equal to AVCaptureSessionPresetHigh.
        let bitrate = Float(videoWith) * Float(videoHeight) * 10.1
        
        saveBitRate = getBitrate(bitrate: bitrate, quality: self.videoQuality)
        
        return getVideoWriterSettings(bitrate: saveBitRate, width: videoWith, height: videoHeight, outputType: outputType)
    }
    
    func buildDefaultAudioOutputSetting() -> [String: Any] {
        var stereoChannelLayout = AudioChannelLayout()
        memset(&stereoChannelLayout, 0, MemoryLayout<AudioChannelLayout>.size)
        stereoChannelLayout.mChannelLayoutTag = kAudioChannelLayoutTag_Stereo
        
        let channelLayoutAsData = Data(bytes: &stereoChannelLayout, count: MemoryLayout<AudioChannelLayout>.size)
        let compressionAudioSettings: [String: Any] = [
            AVFormatIDKey: kAudioFormatMPEG4AAC,
            AVEncoderBitRateKey: 128000,
            AVSampleRateKey: 22050,
            AVChannelLayoutKey: channelLayoutAsData,
            AVNumberOfChannelsKey: 2
        ]
        return compressionAudioSettings
    }
    
    func buildDefaultVideoOutputSetting() throws ->  [String: Any] {
        guard let asset = asset else {
            let eror = NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Asset invalid", comment: "")])
            throw eror
        }
        
        guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
            throw NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("Cannot find video track", comment: "")])
        }
        let bitrate = videoTrack.estimatedDataRate
        if isMinBitRateEnabled && bitrate <= MLMCompressConfiguration.MIN_BITRATE {
            throw NSError(domain: "com.exportsession", code: 0, userInfo: [NSLocalizedDescriptionKey: NSLocalizedString("The provided bitrate is smaller than what is needed for compression try to set isMinBitRateEnabled to false", comment: "")])
        }
        saveBitRate = getBitrate(bitrate: bitrate, quality: self.videoQuality)
        let videoSize = videoTrack.naturalSize
        let size = generateWidthAndHeight(width: videoSize.width, height: videoSize.height, keepOriginalResolution: keepOriginalResolution)
        videoWith = size.width
        videoHeight = size.height
        
        return getVideoWriterSettings(bitrate: saveBitRate, width: videoWith, height: videoHeight, outputType: outputType)
    }
    
    func buildDefaultVideoComposition(with asset: AVAsset) -> AVVideoComposition {
        let videoComposition = AVMutableVideoComposition()
        
        if let videoTrack = asset.tracks(withMediaType: .video).first {
            let trackFrameRate: Float = videoTrack.nominalFrameRate
            videoComposition.frameDuration = CMTime(value: 1, timescale: CMTimeScale(trackFrameRate))
            
            var naturalSize = videoTrack.naturalSize
            var transform = videoTrack.preferredTransform
            let angle = atan2(transform.b, transform.a)
            let videoAngleInDegree = angle * 180 / CGFloat.pi
            if videoAngleInDegree == 90 || videoAngleInDegree == -90 {
                let width = naturalSize.width
                naturalSize.width = naturalSize.height
                naturalSize.height = width
            }
            
            videoComposition.renderSize = naturalSize
            
            var targetSize = naturalSize
            if let width = videoConfiguration.videoOutputSetting?[AVVideoWidthKey] as? NSNumber {
                targetSize.width = CGFloat(width.floatValue)
            }
            if let height = videoConfiguration.videoOutputSetting?[AVVideoHeightKey] as? NSNumber {
                targetSize.height = CGFloat(height.floatValue)
            }
            // Center
            if naturalSize.width > 0 && naturalSize.height > 0 {
                let xratio = targetSize.width / naturalSize.width
                let yratio = targetSize.height / naturalSize.height
                let ratio = min(xratio, yratio)
                let postWidth = naturalSize.width * ratio
                let postHeight = naturalSize.height * ratio
                let transx = (targetSize.width - postWidth) * 0.5
                let transy = (targetSize.height - postHeight) * 0.5
                var matrix = CGAffineTransform(translationX: transx / xratio, y: transy / yratio)
                matrix = matrix.scaledBy(x: ratio / xratio, y: ratio / yratio)
                transform = transform.concatenating(matrix)
            }
            
            let passThroughInstruction = AVMutableVideoCompositionInstruction()
            passThroughInstruction.timeRange = CMTimeRangeMake(start:  CMTime.zero, duration: asset.duration)
            let passThroughLayer = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack)
            passThroughLayer.setTransform(transform, at: CMTime.zero)
            passThroughInstruction.layerInstructions = [passThroughLayer]
            videoComposition.instructions = [passThroughInstruction]
        }
        
        return videoComposition
    }
    
    private func getBitrate(bitrate: Float, quality: VideoQuality) -> Int {
        switch quality {
        case .very_low:
            return Int(bitrate * 0.1)
        case .low:
            return Int(bitrate * 0.3)
        case .medium:
            return Int(bitrate * 0.5)
        case .high:
            return Int(bitrate * 0.7)
        case .very_high:
            return Int(bitrate * 0.9)
        case .original:
            return Int(bitrate)
        }
    }
    
    private func generateWidthAndHeight(
        width: CGFloat,
        height: CGFloat,
        keepOriginalResolution: Bool
    ) -> (width: Int, height: Int) {
        
        if (keepOriginalResolution) {
            return (Int(width), Int(height))
        }
        
        var newWidth: Int
        var newHeight: Int
        
        if width >= 1920 || height >= 1920 {
            
            newWidth = Int(width * 0.5 / 16) * 16
            newHeight = Int(height * 0.5 / 16 ) * 16
            
        } else if width >= 1280 || height >= 1280 {
            newWidth = Int(width * 0.75 / 16) * 16
            newHeight = Int(height * 0.75 / 16) * 16
        } else if width >= 960 || height >= 960 {
            if(width > height){
                newWidth = Int(MLMCompressConfiguration.MIN_HEIGHT * 0.95 / 16) * 16
                newHeight = Int(MLMCompressConfiguration.MIN_WIDTH * 0.95 / 16) * 16
            } else {
                newWidth = Int(MLMCompressConfiguration.MIN_WIDTH * 0.95 / 16) * 16
                newHeight = Int(MLMCompressConfiguration.MIN_HEIGHT * 0.95 / 16) * 16
            }
        } else {
            newWidth = Int(width * 0.9 / 16) * 16
            newHeight = Int(height * 0.9 / 16) * 16
        }
        
        return (newWidth, newHeight)
    }
    
    private func getVideoWriterSettings(bitrate: Int, width: Int, height: Int, outputType: AVVideoCodecType? = .h264) -> [String : AnyObject] {
        
        let videoWriterCompressionSettings = [
            AVVideoAverageBitRateKey : bitrate
        ]
        
        let videoWriterSettings: [String : AnyObject] = [
            AVVideoCodecKey : outputType as AnyObject,
            AVVideoCompressionPropertiesKey : videoWriterCompressionSettings as AnyObject,
            AVVideoWidthKey : width as AnyObject,
            AVVideoHeightKey : height as AnyObject
        ]
        
        return videoWriterSettings
    }
}
