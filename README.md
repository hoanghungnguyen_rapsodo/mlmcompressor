# MLMCompressor

[![CI Status](https://img.shields.io/travis/Hung Nguyen/MLMCompressor.svg?style=flat)](https://travis-ci.org/Hung Nguyen/MLMCompressor)
[![Version](https://img.shields.io/cocoapods/v/MLMCompressor.svg?style=flat)](https://cocoapods.org/pods/MLMCompressor)
[![License](https://img.shields.io/cocoapods/l/MLMCompressor.svg?style=flat)](https://cocoapods.org/pods/MLMCompressor)
[![Platform](https://img.shields.io/cocoapods/p/MLMCompressor.svg?style=flat)](https://cocoapods.org/pods/MLMCompressor)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Usage
In order to use the compressor, just call **export()** and pass source and file name.

You can pass one of 6 video qualities (default is **.medium**): **.original**, **.very_high**, **.high**, .**medium**, **.low** or **.very_low** and the package will handle generating the right bitrate and size values for the output video. You can choose the output file type (default is **.hevc**). 

**isMinBitRateEnabled** to determine if the checking for a minimum bitrate threshold
 before compression is enabled or not. This default to `false`.

 **keepOriginalResolution** to keep the original video height and width when compressing. This defaults to `true`

The method has a callback for 5 functions:

 *  onStart - called when compression started.
 *  onSuccess - called when compression completed with no errors/exceptions. Return **VideoResult** that contain all the video informations including **thumbnail** in JPEG format.
 *  onFailure - called when an exception occurred or video bitrate and size are below the minimum required for compression.
 *  onProgress - called with progress new value.
 *  onCancelled - called when the job is cancelled.

You can cancel anytime by calling **cancelExport()**

You can also remove the video by calling **removeFileAt(URL)** or remove all the videos **removeAllFile()**.

You can record video with **capture(CMSampleBuffer)**.

Return object **VideoResult** with: 

*  saveBitRate: bitrate of compressed video.
*  videoWith and videoHeight: resolution size.
*  thumbnail: thumbnail of compressed video in JPEG format.
*  asset: compressed video assset in AVAsset format.
*  fileSize: file size.
*  duration: duration in second.
*  videoEncodeType: compressed video encode (H262 or HEVC).
*  videoQuality: compressed video quality .

**Quality** support: 

*  original // keep 100% bitrate.
*  case very_high // keep 90% bitrate.
*  high // keep 70% bitrate.
*  medium // keep 50% bitrate.
*  low // keep 30% bitrate.
*  very_low // keep 10% bitrate.

Compress from another encoder to HEVC, the option high or medium is recommended.
Referent Articles: https://larryjordan.com/articles/when-to-use-h-264-vs-hevc-for-video-compression/

You can create your own thumbnail with custom time using **generateThumbnailFromAsset(AVAsset, Float64)**.

## Installation

MLMCompressor is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:
Adding the standard cocoapods at the top

source 'https://github.com/CocoaPods/Specs.git'

```ruby
pod 'MLMCompressor', '1.5.0'
```


## Compatibility
The minimum iOS version supported is 12.

## License

MLMCompressor is available under the MIT license. See the LICENSE file for more info.
